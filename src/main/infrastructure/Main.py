import datetime

from delivery.CommandInterpreter import CommandInterpreter
from delivery.ConsoleImplementation import ConsoleImplementation
from delivery.ConsolePresenter import ConsolePresenter
from delivery.TimeReadableParser import TimeReadableParser
from infrastructure.InMemoryMessages import InMemoryMessages
from infrastructure.InMemoryUsers import InMemoryUsers
from usecase.Follow import Follow
from usecase.Post import Post
from usecase.Read import Read
from usecase.Wall import Wall

if __name__ == "__main__":
    messages = InMemoryMessages()
    users = InMemoryUsers()
    clock = datetime.datetime
    presenter = ConsolePresenter(TimeReadableParser(clock))
    console = ConsoleImplementation(presenter)
    commands = {'read': Read(messages),
                'post': Post(messages, clock),
                'follow': Follow(users),
                'wall': Wall(messages, users)}
    presenter.interpreter = CommandInterpreter(commands, presenter)
    presenter.console = console
    console.read_user_input()
