from domain import User
from domain.Users import Users


class InMemoryUsers(Users):
    def __init__(self):
        self.users = {}

    def find_user(self, username):
        keys = self.users.get(username)
        if keys is not None:
            return User.create(username, keys['following'])
        else:
            return User.create(username)

    def put(self, user):
        username = user.username
        following = user.following
        self.users[username] = {'following': following}
