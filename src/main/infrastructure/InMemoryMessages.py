from domain.Message import Message
from domain.Messages import Messages


class InMemoryMessages(Messages):
    def __init__(self):
        self.stored_messages = {}

    def add(self, message: Message):
        if message.username in self.stored_messages:
            messages = self.stored_messages[message.username]
            messages.append(self.parse_msg(message))
        else:
            self.stored_messages[message.username] = [self.parse_msg(message)]

    def parse_msg(self, m: Message):
        return {'message': m.message, 'time': m.datetime}

    def find_posted_by(self, username):
        user_messages = self.stored_messages.get(username)
        if user_messages is not None:
            return [Message.create(username, m['message'], m['time']) for m in user_messages]
        else:
            return []
