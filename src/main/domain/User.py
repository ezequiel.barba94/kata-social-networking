class User:
    def __init__(self, username: str, following: [str]):
        self.username = username
        self.following = following

    def follow(self, user):
        self.following.append(user.username)


def create(username, following=None):
    if following is None:
        following = []
    return User(username, following)
