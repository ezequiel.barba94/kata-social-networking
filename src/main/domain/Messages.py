from abc import ABC, abstractmethod

from domain import Message


class Messages(ABC):
    @abstractmethod
    def add(self, message: Message):
        pass

    @abstractmethod
    def find_posted_by(self, username):
        pass
