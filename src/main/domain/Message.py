from datetime import datetime


class Message:
    def __init__(self, username: str, message: str, time: datetime):
        self.username = username
        self.message = message
        self.datetime = time

    @staticmethod
    def create(username: str, message: str, time: datetime):
        return Message(username, message, time)
