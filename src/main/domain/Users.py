from abc import ABC, abstractmethod

from domain import User


class Users(ABC):
    @abstractmethod
    def find_user(self, username) -> User:
        pass

    @abstractmethod
    def put(self, username):
        pass
