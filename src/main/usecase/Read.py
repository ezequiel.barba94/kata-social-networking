class Read:
    def __init__(self, messages):
        self.messages = messages

    def execute(self, username: str):
        return self.messages.find_posted_by(username)
