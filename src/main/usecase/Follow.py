from domain import Users


class Follow:
    def __init__(self, users: Users):
        self.users = users

    def execute(self, follower: str, followed: str):
        user_who_follows = self.users.find_user(follower)
        user_who_is_followed = self.users.find_user(followed)
        user_who_follows.follow(user_who_is_followed)
        self.users.put(user_who_follows)
