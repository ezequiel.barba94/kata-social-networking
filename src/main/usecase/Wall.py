from domain import Messages, Users


class Wall:
    def __init__(self, messages: Messages, users: Users):
        self.users = users
        self.messages = messages

    def execute(self, username):
        user = self.users.find_user(username)
        messages = self.messages.find_posted_by(username)
        for username in user.following:
            messages += self.messages.find_posted_by(username)
        return list(reversed(sorted(messages, key=lambda msg: msg.datetime)))
