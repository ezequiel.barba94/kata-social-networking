from domain.Message import Message


class Post:
    def __init__(self, messages, clock):
        self.messages = messages
        self.clock = clock

    def execute(self, username: str, message: str):
        post = Message(username, message, self.clock.now())
        self.messages.add(post)
