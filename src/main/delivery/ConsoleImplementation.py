from delivery.ConsolePresenter import ConsolePresenter


class ConsoleImplementation:
    def __init__(self, presenter: ConsolePresenter):
        self.presenter = presenter

    def read_user_input(self):
        while True:
            command = input('> ')
            self.presenter.user_inputs(command)

    def print_lines(self, lines):
        for line in lines:
            print(line)
