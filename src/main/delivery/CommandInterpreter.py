from typing import List

from delivery.ConsolePresenter import ConsolePresenter


class CommandInterpreter:
    def __init__(self, commands, presenter: ConsolePresenter):
        self.commands = commands
        self.presenter = presenter

    def interpret(self, command: str):
        tokens = command.split()
        if len(tokens) == 1:
            self.read(tokens)
        elif len(tokens) == 2 and tokens[1] == 'wall':
            self.wall(tokens)
        elif tokens[1] == '->':
            self.post(tokens)
        elif tokens[1] == 'follows':
            self.follow(tokens)

    def post(self, tokens: List[str]):
        username, message = tokens[0], ' '.join(tokens[2:])
        self.commands['post'].execute(username, message)

    def wall(self, tokens):
        username = tokens[0]
        messages = self.commands['wall'].execute(username)
        self.presenter.show_wall(messages)

    def read(self, tokens):
        username = tokens[0]
        messages = self.commands['read'].execute(username)
        self.presenter.show_timeline(messages)

    def follow(self, tokens):
        follower = tokens[0]
        followed = tokens[2]
        self.commands['follow'].execute(follower, followed)
