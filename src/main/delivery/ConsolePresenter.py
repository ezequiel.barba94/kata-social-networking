from domain import Message


class ConsolePresenter:
    def __init__(self, time_parser):
        self.interpreter = None
        self.console = None
        self.time_parser = time_parser

    def user_inputs(self, command):
        self.interpreter.interpret(command)

    def convert_to_timeline_line(self, message: Message):
        user_message = message.message
        elapsed_time = self.time_parser.parse(message.datetime)
        return f'{user_message} ({elapsed_time})'

    def show_timeline(self, messages: [Message]):
        self.console.print_lines([self.convert_to_timeline_line(m) for m in messages])

    def show_wall(self, messages: [Message]):
        self.console.print_lines([self.convert_to_wall_line(m) for m in messages])

    def convert_to_wall_line(self, message: Message):
        username = message.username
        return f'{username} - {self.convert_to_timeline_line(message)}'
