from datetime import timedelta


class TimeReadableParser:

    def __init__(self, clock):
        self.clock = clock

    def parse(self, time):
        delta: timedelta = self.clock.now() - time
        if delta.days >= 2:
            return f'{delta.days} days ago'
        elif delta.days == 1:
            return '1 day ago'
        elif delta.seconds // 3600 >= 2:
            return f'{delta.seconds // 3600} hours ago'
        elif delta.seconds // 3600 == 1:
            return '1 hour ago'
        elif delta.seconds // 60 >= 2:
            return f'{delta.seconds // 60} minutes ago'
        elif delta.seconds // 60 == 1:
            return '1 minute ago'
        elif delta.seconds >= 2:
            return f'{delta.seconds} seconds ago'
        elif delta.seconds == 1:
            return '1 second ago'
