class StoppedClock():

    def __init__(self, moment):
        self.moment = moment

    def now(self):
        return self.moment
