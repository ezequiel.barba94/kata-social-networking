from datetime import datetime

from utils.StoppedClock import StoppedClock


def default_stopped_clock():
    return StoppedClock(datetime(2000, 6, 15, 12, 30, 30))


TWO_DAYS_AGO = datetime(2000, 6, 13, 12, 30, 30)
A_DAY_AGO = datetime(2000, 6, 14, 12, 30, 30)
ONE_HOUR_AGO = datetime(2000, 6, 15, 11, 29, 30)
FOUR_HOURS_AGO = datetime(2000, 6, 15, 8, 30, 30)
FIVE_MINUTES_AGO = datetime(2000, 6, 15, 12, 25, 30)
ONE_MINUTE_AGO = datetime(2000, 6, 15, 12, 29, 30)
TWENTY_SECONDS_AGO = datetime(2000, 6, 15, 12, 30, 10)
ONE_SECOND_AGO = datetime(2000, 6, 15, 12, 30, 29)
