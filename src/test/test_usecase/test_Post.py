import unittest
from datetime import datetime

from infrastructure.InMemoryMessages import InMemoryMessages
from usecase.Post import Post
from utils.StoppedClock import StoppedClock


class PostTests(unittest.TestCase):
    POSTED_TIME = datetime(2000, 1, 20, 13)

    def test_posted_messages_are_saved(self):
        messages = InMemoryMessages()
        post = Post(messages, StoppedClock(self.POSTED_TIME))

        post.execute('alice', 'message')
        post.execute('alice', 'another message')

        posted_messages = messages.find_posted_by('alice')
        self.assertListEqual(['message', 'another message'], [m.message for m in posted_messages])

    def test_posted_messages_with_time(self):
        messages = InMemoryMessages()
        post = Post(messages, StoppedClock(self.POSTED_TIME))

        post.execute('alice', 'message')

        posted_messages = messages.find_posted_by('alice')
        self.assertIn(self.POSTED_TIME, [m.datetime for m in posted_messages])
