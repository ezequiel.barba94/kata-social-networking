import unittest

from domain.Message import Message
from infrastructure.InMemoryMessages import InMemoryMessages
from infrastructure.InMemoryUsers import InMemoryUsers
from usecase.Follow import Follow
from usecase.Wall import Wall
from utils.Moment import FIVE_MINUTES_AGO, FOUR_HOURS_AGO, TWENTY_SECONDS_AGO


class WallTests(unittest.TestCase):
    def test_shows_user_posted_messages(self):
        self.messages.add(Message('Username', 'message', FIVE_MINUTES_AGO))

        user_wall = self.wall.execute('Username')

        self.assertListEqual(['message'], [m.message for m in user_wall])
        self.assertListEqual(['Username'], [m.username for m in user_wall])

    def test_shows_subscriptions_messages(self):
        self.messages.add(Message('Another', 'five minutes ago', FIVE_MINUTES_AGO))
        self.messages.add(Message('Username', 'four hours ago', FOUR_HOURS_AGO))
        self.messages.add(Message('Username', 'twenty seconds ago', TWENTY_SECONDS_AGO))
        self.follow.execute('Username', 'Another')

        user_wall = self.wall.execute('Username')

        expected_messages = ['twenty seconds ago', 'five minutes ago', 'four hours ago']
        self.assertListEqual(expected_messages, [m.message for m in user_wall])

    def setUp(self) -> None:
        self.messages = InMemoryMessages()
        self.users = InMemoryUsers()
        self.follow = Follow(self.users)
        self.wall = Wall(self.messages, self.users)
