import unittest

from domain import Users
from infrastructure.InMemoryUsers import InMemoryUsers
from usecase.Follow import Follow


class FollowTests(unittest.TestCase):
    def test_follow_user(self):
        users: Users = InMemoryUsers()
        follow = Follow(users)

        follow.execute('alice', 'bob')

        user = users.find_user('alice')
        self.assertListEqual(['bob'], user.following)
