import unittest
from datetime import datetime

from infrastructure.InMemoryMessages import InMemoryMessages
from usecase.Post import Post
from usecase.Read import Read
from utils.StoppedClock import StoppedClock


class ReadTests(unittest.TestCase):
    USER = 'user'
    USER_MESSAGE = 'user message'
    POSTED_TIME = datetime(2000, 1, 20, 13, 0, 0)

    def test_show_posted_messages(self):
        read = self.given_posted_messages()

        wall_messages = read.execute(self.USER)

        self.assertIn(self.USER_MESSAGE, [m.message for m in wall_messages])

    def test_shows_messages_with_time(self):
        read = self.given_posted_messages()

        wall_messages = read.execute(self.USER)

        self.assertIn(self.POSTED_TIME, [m.datetime for m in wall_messages])

    def test_shows_nothing_if_no_message_is_posted(self):
        read = self.given_no_messages()

        messages = read.execute('AnotherUser')

        self.assertListEqual([], messages)

    def given_posted_messages(self):
        messages = InMemoryMessages()
        post = Post(messages, StoppedClock(self.POSTED_TIME))
        post.execute(self.USER, self.USER_MESSAGE)
        read = Read(messages)
        return read

    def given_no_messages(self):
        messages = InMemoryMessages()
        return Read(messages)
