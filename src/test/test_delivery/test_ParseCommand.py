import unittest
from unittest.mock import Mock

from delivery.CommandInterpreter import CommandInterpreter
from delivery.ConsolePresenter import ConsolePresenter


class ParseCommandTests(unittest.TestCase):
    def test_read_command_is_parsed(self):
        self.interpreter.interpret('Username')
        self.commands['read'].execute.assert_called_once_with('Username')
        self.presenter.show_timeline.assert_called_once()

    def test_wall_command_is_parsed(self):
        self.interpreter.interpret('Username wall')
        self.commands['wall'].execute.assert_called_once_with('Username')
        self.presenter.show_wall.assert_called_once()

    def test_post_command_is_parsed(self):
        self.interpreter.interpret('Username -> message')
        self.commands['post'].execute.assert_called_once_with('Username', 'message')

    def test_post_command_with_multiple_words_is_parsed(self):
        self.interpreter.interpret('Username -> message with multiple words')
        self.commands['post'].execute.assert_called_once_with('Username', 'message with multiple words')

    def test_follow_command_is_parsed(self):
        self.interpreter.interpret('Username follows AnotherUser')
        self.commands['follow'].execute.assert_called_once_with('Username', 'AnotherUser')

    def setUp(self) -> None:
        self.commands = {'wall': Mock(), 'read': Mock(), 'post': Mock(), 'follow': Mock()}
        self.presenter: ConsolePresenter = Mock()
        self.interpreter = CommandInterpreter(self.commands, self.presenter)
