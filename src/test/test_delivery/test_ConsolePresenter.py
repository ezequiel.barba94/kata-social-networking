import unittest
from unittest.mock import Mock

from delivery.ConsolePresenter import ConsolePresenter
from domain.Message import Message


class ConsolePresenterTests(unittest.TestCase):

    def test_user_inputs_communicates_with_interpreter(self):
        self.presenter.user_inputs("Alice")

        self.presenter.interpreter.interpret.assert_called_with("Alice")

    def test_shows_timeline_on_console(self):
        self.presenter.show_timeline(self.MESSAGE)

        self.console.print_lines.assert_called_once_with(['message (2 days ago)'])

    def test_wall_shows_messages_with_username(self):
        self.presenter.show_wall(self.MESSAGE)

        self.console.print_lines.assert_called_once_with(['alice - message (2 days ago)'])

    def setUp(self) -> None:
        self.console = Mock()
        time_parser = Mock()
        time_parser.parse.side_effect = lambda _: "2 days ago"
        self.presenter = ConsolePresenter(time_parser)
        self.presenter.console = self.console
        self.presenter.interpreter = Mock()
        self.MESSAGE = [Message('alice', 'message', Mock())]
