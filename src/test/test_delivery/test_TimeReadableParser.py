import unittest

from delivery.TimeReadableParser import TimeReadableParser
from utils.Moment import *


class TimeReadableParserTests(unittest.TestCase):

    def test_shows_two_days_ago_message(self):
        parsed_time = self.time_parser.parse(TWO_DAYS_AGO)

        self.assertEqual('2 days ago', parsed_time)

    def test_shows_one_day_ago_posted_time(self):
        parsed_time = self.time_parser.parse(A_DAY_AGO)

        self.assertEqual('1 day ago', parsed_time)

    def test_shows_five_minutes_ago_message(self):
        parsed_time = self.time_parser.parse(FIVE_MINUTES_AGO)

        self.assertEqual('5 minutes ago', parsed_time)

    def test_shows_one_minute_ago_message(self):
        parsed_time = self.time_parser.parse(ONE_MINUTE_AGO)

        self.assertEqual('1 minute ago', parsed_time)

    def test_shows_four_hours_ago_message(self):
        parsed_time = self.time_parser.parse(FOUR_HOURS_AGO)

        self.assertEqual('4 hours ago', parsed_time)

    def test_shows_one_hour_ago_message(self):
        parsed_time = self.time_parser.parse(ONE_HOUR_AGO)

        self.assertEqual('1 hour ago', parsed_time)

    def test_shows_twenty_seconds_ago_message(self):
        parsed_time = self.time_parser.parse(TWENTY_SECONDS_AGO)

        self.assertEqual('20 seconds ago', parsed_time)

    def test_shows_one_second_ago_message(self):
        parsed_time = self.time_parser.parse(ONE_SECOND_AGO)

        self.assertEqual('1 second ago', parsed_time)

    def setUp(self) -> None:
        self.time_parser = TimeReadableParser(default_stopped_clock())
